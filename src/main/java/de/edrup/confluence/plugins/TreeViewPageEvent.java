package de.edrup.confluence.plugins;

import java.io.Serializable;
import java.util.Date;

public class TreeViewPageEvent implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private final Long oldParentID;
	private final Long timerTick;
	
	public TreeViewPageEvent(Long oldParentID) {
		this.oldParentID = oldParentID;
		Date d = new Date();
		this.timerTick = d.getTime();
	}
	
	public Long getOldParentID() {
		return oldParentID;
	}
	
	public Long getTimerTick() {
		return timerTick;
	}
}

package de.edrup.confluence.plugins;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class StringResponse {
	
	@XmlElement(name = "result")
	private String response;
	
	public StringResponse() {
	}
	
	public StringResponse(String response) {
		this.response = response;
	}
}

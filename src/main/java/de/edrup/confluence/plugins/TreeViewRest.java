package de.edrup.confluence.plugins;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.user.UserKey;

@Path("/")
public class TreeViewRest {
	
	private final UserAccessor userAcc;
	private final TreeViewHelper treeViewHelper;
	private final PageManager pageMan;
	private final PermissionManager permissionMan;
	
	private Page p;
	private ConfluenceUser user;
	
	
	public TreeViewRest(UserAccessor userAcc, TreeViewHelper treeViewHelper, PageManager pageMan,
			PermissionManager permissionMan) {
		this.userAcc = userAcc;
		this.treeViewHelper = treeViewHelper;
		this.pageMan = pageMan;
		this.permissionMan = permissionMan;
	}

	
	// check if the page ID given is covered by a tree watch of the user
	// in case username is not given the calling user is used
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/isCovered")
    public Response isCovered(@QueryParam("username") String username, @PathParam("id") Long contentId)
    {
		// do our pre-handling
		Response r = handleParamsAndCheckRights(username, contentId);
		if(r.getStatus() != Status.OK.getStatusCode()) {
			return r;
		}
		
		// check if page is affected by any watch of the given user
		Long treeWatchID = treeViewHelper.isUserTreeWatchingPage(p, user);
		if(treeWatchID > 0L) {
			return Response.ok(new StringResponse("true;" + treeWatchID.toString())).build();
		}
		else {
			return Response.ok(new StringResponse("false")).build();
		}
    }
	
	
	// set a tree watch for the given page ID and user
	// in case username is not given the calling user is used
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/watchTree")
    public Response watchTree(@QueryParam("username") String username, @PathParam("id") Long contentId)
    {
		// doour pre-handling
		Response r = handleParamsAndCheckRights(username, contentId);
		if(r.getStatus() != Status.OK.getStatusCode()) {
			return r;
		}
		
		// set the watch flag for this page an all descendants
		treeViewHelper.setWatchFlagInTransaction(user, pageMan.getPage(contentId), true);
					
		// add the tree view to our list
		treeViewHelper.addWatch(user, pageMan.getPage(contentId));
		
		// return success
		return Response.ok(new StringResponse("success")).build();
    }
	
	
	// remove a tree watch for the given page ID and user
	// in case username is not given the calling user is used
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/unwatchTree")
    public Response unwatchTree(@QueryParam("username") String username, @PathParam("id") Long contentId)
    {
		// do our pre-handling
		Response r = handleParamsAndCheckRights(username, contentId);
		if(r.getStatus() != Status.OK.getStatusCode()) {
			return r;
		}
		
		// set the watch flag for this page an all descendants
		treeViewHelper.setWatchFlagInTransaction(user, pageMan.getPage(contentId), false);
					
		// add the tree view to our list
		treeViewHelper.removeWatch(user, pageMan.getPage(contentId));
		
		// repond success
		return Response.ok(new StringResponse("success")).build();
    }
	
	
	// return all tree watches (of a user)
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/getTreeWatches")
	public Response getTreeWatches(@QueryParam("username") String username) {
		
		// action only allowed for admins
		if(!permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get())) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		// our array list of watches
		ArrayList<WatchResponse> watchesREST = new ArrayList<WatchResponse>();
		
		// get all tree watches
		ArrayList<String> watches = new ArrayList<String>();
		watches.addAll(treeViewHelper.getWatchesAsArray());
		
		// loop through all watches
		for(String watch:watches) {
			
			// determine the confluence user from the watch
			ConfluenceUser confUser = userAcc.getExistingUserByKey(new UserKey(treeViewHelper.getUserKeyFromWatch(watch)));
			
			// determine the page ID from the watch
			Long pageID = treeViewHelper.getPageIDFromWatch(watch);
			
			if(confUser != null) {
				// in case the username is given as parameter
				if(username != null) {
					// check whether the watch user equals the username
					if(confUser.getName().equals(username)) {
						watchesREST.add(new WatchResponse(pageID, confUser.getName()));
					}
				}
				// in case the username is not given as parameter we simply return all watches
				else {
					// add the watch to our response
					watchesREST.add(new WatchResponse(pageID, confUser.getName()));
				}
			}
		}

		return Response.ok(new WatchesResponse(watchesREST)).build();
	}
	
	
	// return all of my tree watches
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/getMyTreeWatches")
	public Response getMyTreeWatches() {
		
		user = AuthenticatedUserThreadLocal.get();
			
		// our array list of watches
		ArrayList<WatchResponse> watchesREST = new ArrayList<WatchResponse>();
		
		// get all tree watches
		ArrayList<String> watches = new ArrayList<String>();
		watches.addAll(treeViewHelper.getWatchesAsArray());
		
		// loop through all watches
		for(String watch:watches) {
			
			// determine the confluence user from the watch
			ConfluenceUser confUser = userAcc.getExistingUserByKey(new UserKey(treeViewHelper.getUserKeyFromWatch(watch)));
			
			// determine the page ID from the watch
			Long pageID = treeViewHelper.getPageIDFromWatch(watch);
			
			if(confUser != null) {
				// check whether the watch user equals the username
				if(confUser.equals(user)) {
					watchesREST.add(new WatchResponse(pageID, confUser.getName()));
				}
			}
		}

		return Response.ok(new WatchesResponse(watchesREST)).build();
	}
	
	
	// test call
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/test")
    public Response Test()
    {
		return Response.ok(new StringResponse("Hello this is the Tree View REST interface!")).build();
    }
	
	
	// handle the parameters username and contentId => user, p and check the proper rights of this REST request
	private Response handleParamsAndCheckRights(String username, Long contentId) {
		// the user for which we going to check
		user = (username == null) ? AuthenticatedUserThreadLocal.get() : userAcc.getUserByName(username);
		
		// get the page corresponding to the given ID
		p = pageMan.getPage(contentId);
		
		// negative response in case the page or user does not exist
		if((p == null) || (user == null)) {
			return Response.status(Status.NOT_FOUND).build();
		}
		else {
			// check whether user is allowed to view the page and whether a non admin asks for a another user
			// remark: "another" is not checked here directly - once the username is specified we assume an admin call
			if(!permissionMan.hasPermission(user, Permission.VIEW, p) || ((username != null) &&
					!permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get()))) {
				return Response.status(Status.FORBIDDEN).build();
			}
			// everything ok
			else {
				return Response.status(Status.OK).build();
			}
		}
	}
}